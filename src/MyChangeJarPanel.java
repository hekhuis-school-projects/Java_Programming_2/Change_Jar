import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Creates a GUI for the ChangeJar class.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class MyChangeJarPanel extends JPanel {
	
	//Declaration of ChangeJars
	private ChangeJar[] myJars;
	
	//Declaration of buttons
	private JButton add1, add2, add3,
					subtract1, subtract2, subtract3,
					lock, unlock;
	
	//Declaration of labels
	private JLabel j1, j2, j3;
	
	//Declaration of text fields
	private JTextField t1, t2, t3;
	
	public MyChangeJarPanel() {
		
		//Jars
		myJars = new ChangeJar[3];
		myJars[0] = new ChangeJar(2.45);
		myJars[1] = new ChangeJar("5.26");
		myJars[2] = new ChangeJar(20, 3, 1, 5);
		
		//Buttons
		add1 = new JButton("ADD");
		add2 = new JButton("ADD");
		add3 = new JButton("ADD");
		subtract1 = new JButton("SUBTRACT");
		subtract2 = new JButton("SUBTRACT");
		subtract3 = new JButton("SUBTRACT");
		lock = new JButton("LOCK JARS");
		unlock = new JButton("UNLOCK JARS");
		
		//Jar Labels
		j1 = new JLabel("JAR 1 => " + myJars[0].toString());
		j2 = new JLabel("JAR 2 => " + myJars[1].toString());
		j3 = new JLabel("JAR 3 => " + myJars[2].toString());

		//Text Fields
		t1 = new JTextField(10);
		t2 = new JTextField(10);
		t3 = new JTextField(10);
		
		
		
		//Add Action Listener
		ButtonListener listener = new ButtonListener();
		
		add1.addActionListener(listener);
		add2.addActionListener(listener);
		add3.addActionListener(listener);
		subtract1.addActionListener(listener);
		subtract2.addActionListener(listener);
		subtract3.addActionListener(listener);
		lock.addActionListener(listener);
		unlock.addActionListener(listener);
		
		//Set Layout and add buttons / labels
		setLayout(new GridLayout(4,4));
		
		add(j1);
		add(add1);
		add(subtract1);
		add(t1);
		add(j2);
		add(add2);
		add(subtract2);
		add(t2);
		add(j3);
		add(add3);
		add(subtract3);
		add(t3);
		add(lock);
		add(unlock);
	}
	
	private class ButtonListener implements ActionListener {
	
		public void actionPerformed(ActionEvent event) {
			Object source = event.getSource();
			double temp;
			JFrame frame = new JFrame("Error");
			
			if(source == add1) {
				try {
					temp = Double.parseDouble(t1.getText());
					myJars[0].add(temp);
					j1.setText("JAR 1 => " + myJars[0].toString());
					t1.setText("");
				}
				catch(IllegalArgumentException e) {
					
					JOptionPane.showMessageDialog(frame, e);
				}
			}
			else if(source == add2) {
				try {
					temp = Double.parseDouble(t2.getText());
					myJars[1].add(temp);
					j2.setText("JAR 2 => " + myJars[1].toString());
					t2.setText("");
				}
				catch(IllegalArgumentException e) {
					
					JOptionPane.showMessageDialog(frame, e);
				}
			}
			else if(source == add3) {
				try {
					temp = Double.parseDouble(t3.getText());
					myJars[2].add(temp);
					j3.setText("JAR 3 => " + myJars[2].toString());
					t3.setText("");
				}
				catch(IllegalArgumentException e) {
					
					JOptionPane.showMessageDialog(frame, e);
				}
			}
			else if(source == subtract1) {
				try {
					temp = Double.parseDouble(t1.getText());
					myJars[0].subtract(temp);
					j1.setText("JAR 1 => " + myJars[0].toString());
					t1.setText("");
				}
				catch(IllegalArgumentException e) {
					
					JOptionPane.showMessageDialog(frame, e);
				}
			}
			else if(source == subtract2) {
				try {
					temp = Double.parseDouble(t2.getText());
					myJars[1].subtract(temp);
					j2.setText("JAR 2 => " + myJars[1].toString());
					t2.setText("");
				}
				catch(IllegalArgumentException e) {
					
					JOptionPane.showMessageDialog(frame, e);
				}
			}
			else if(source == subtract3) {
				try {
					temp = Double.parseDouble(t3.getText());
					myJars[2].subtract(temp);
					j3.setText("JAR 3 => " + myJars[2].toString());
					t3.setText("");
				}
				catch(IllegalArgumentException e) {
					
					JOptionPane.showMessageDialog(frame, e);
				}
			}
			else if(source == lock) {
				ChangeJar.lock(true);
			}
			else if(source == unlock) {
				ChangeJar.lock(false);
			}
		}
		
	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("My Change Jars - Kyle Hekhuis");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MyChangeJarPanel panel = new MyChangeJarPanel();
		frame.getContentPane().add(panel);
		
		frame.pack();
		frame.setVisible(true);
	}
}
 