import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.Scanner;

/**
 * The ChangeJar class represents a coin jar that holds 
 * quarters, dimes, nickels, and pennies.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class ChangeJar {

	//Declaration of coins and lock
    private int quarters, dimes, nickels, pennies;
    private static boolean globalLock = false;

    /**
     * Constructs a new ChangeJar object with 
     * quarters, dimes, nickels, and pennies set to 0.
     */
    public ChangeJar() {
        this(0,0,0,0);
    }

    /**
     * Constructs a new ChangeJar object with quarters, dimes,
     * nickels, and pennies set to the given parameter values.
     * 
     * @param quarters number of quarters
     * @param dimes number of dimes
     * @param nickels number of nickels
     * @param pennies number of pennies
     * 
     * @throws IllegalArgumentException for invalid parameter values
     */
    public ChangeJar(int quarters, int dimes, int nickels, int pennies) {
    	if(quarters >= 0 && dimes >= 0 && nickels >= 0 && pennies >= 0) {
	        this.quarters = quarters;
	        this.dimes = dimes;
	        this.nickels = nickels;
	        this.pennies = pennies;
    	} else {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
    }

    /**
     * Constructs a new ChangeJar object and uses the parameter
     * value to set the number of quarters,dimes, nickels,
     * and pennies (in that order) for this coin jar.
	 *
     * @param amount initial amount to use to set number of coins in the jar
     * 
     * @throws IllegalArgumentException if the parameter value is invalid
     */
    public ChangeJar(final double amount) {
        if(isValidDouble(amount)) {
        	converter(amount);
        } else {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
    }

    /**
     * Constructs a new ChangeJar object and uses the parameter
     * value to set the number of quarters, dimes, nickels, and 
     * pennies (in that order) for this coin jar.
     * 
     * @param amount initial amount to use to set number of coins in the jar
     * 
     * @throws IllegalArgumentException if the parameter value is invalid
     */
    public ChangeJar(final String amount) {
    	if(amount == null) {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
    	if(isValidString(amount)) {
        	try {
        		double temp = Double.parseDouble(amount);
        		converter(temp);
        	}
        	catch(NumberFormatException e) {
        		throw new IllegalArgumentException("Invalid value for parameter.");
        	}
    	} else {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
        
    }

    /**
     * Constructs a new ChangeJar object and uses the 
     * values of the parameter ChangeJar object to
     * initialize the number of coins for this object.
     * 
     * @param other the ChangeJar object used to set the number of
     * coins in this object
     * 
     * @throws IllegalArgumentException if the parameter value is invalid
     */
    public ChangeJar(final ChangeJar other) {
    	if(other != null) {
    		this.quarters = other.quarters;
    		this.dimes = other.dimes;
    		this.nickels = other.nickels;
    		this.pennies = other.pennies;
    	} else {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
    }

    /**
     * Converts given amount into the least amount of coins
     * and sets instance variables
     * 
     * @param amount amount to break up into coins
     */
    private void converter(double amount) {
    	amount *= 100;
    	this.quarters = (int)(amount / 25);
        this.dimes = (int)( (amount % 25) / 10);
        this.nickels = (int)( ( (amount % 25) % 10) / 5);
        this.pennies = (int)Math.round( ( ( ( (amount % 25) % 10) % 5) / 1) );
    }
    
    /**
     * Checks if the amount is a valid input by 
     * seeing if it's in currency notation (without the $)
     * i.e. 0.00
     * 
     * @param amount amount to check validity of
     * 
     * @return true if it is a valid number (e.g. 10.49), 
     * false otherwise
     */
    private boolean isValidDouble(double amount) {
    	String temp = Double.toString(amount);
    	if((amount * 100) % 10 == 0) {
    		temp += "0";
    	}
    	return isValidString(temp);
    }
    
    /**
     * Checks if the amount is a valid input by 
     * seeing if it's in currency notation (without the $)
     * i.e. 0.00
     * 
     * @param amount amount to check validity of
     * 
     * @return true if it is a valid number (e.g. 10.49), 
     * false otherwise
     */
    private boolean isValidString(String amount) {
    	if(!amount.contains(".") || amount.contains("-")) {
    		return false;
    	}
    	int index = amount.indexOf(".");
    	if(amount.length() == index + 3) {
    		return true;
    	} else {
    		return false;
    	}
    }

    /**
     * Returns the number of quarters in this jar.
     * 
     * @return number of quarters
     */
    public int getQuarters() {
        return quarters;
    }

    /**
     * Returns the number of dimes in this jar.
     * 
     * @return number of dimes
     */
    public int getDimes() {
        return dimes;
    }

    /**
     * Returns the number of nickels in this jar.
     * 
     * @return number of nickels
     */
    public int getNickels() {
        return nickels;
    }

    /**
     * Returns the number of pennies in this jar.
     * 
     * @return number of pennies
     */
    public int getPennies() {
        return pennies;
    }

    /**
     * Returns total amount of money in this jar.
     * 
     * @return total amount of money
     */
    public double getAmount() {
        double amount = (double) Math.round( ( (quarters * .25) + (dimes * .1) + (nickels * .05) + (pennies * .01) ) * 100) / 100;
        return amount;
    }

    /**
     * Compares this object to the given object. The result is true
     * if and only if the argument is not null and is an ChangeJar 
     * object and the total amount of the specified ChangeJar is 
     * equal to the total amount of this ChangeJar object. It 
     * returns false otherwise.
     * 
     * @param other the object to compare with
     * 
     * @return true if the given object is equal (as defined above)
     * to this object, false otherwise
     */
    public boolean equals(Object other) {
        if(other == null || !(other instanceof ChangeJar)) {
            return false;
        }
        ChangeJar temp = (ChangeJar) other;
        return compareTo(temp) == 0;
    }

    /**
     * Compares this object to the given ChangeJar object. The result 
     * is true if and only if the argument is not null and the total 
     * amount of the specified ChangeJar is equal to the total amount 
     * of this ChangeJar object.
     * 
     * @param other The ChangeJar object to compare this ChangeJar object against
     * 
     * @return true if the given object is equal (as defined above) to 
     * this object, false otherwise
     */
    public boolean equals(ChangeJar other) {
        return compareTo(other) == 0;
    }

    /**
     * Compares the two given ChangeJar objects. The result is true 
     * if and only if the total amount in the first ChangeJar object 
     * is equal to the total amount in the second ChangeJar object.
     * 
     * @param obj1 The ChangeJar object used in comparison
     * @param obj2 The ChangeJar object used in comparison
     * 
     * @return true if the two objects are equal, false otherwise
     */
    public static boolean equals(ChangeJar obj1, ChangeJar obj2) {
    	return obj1.equals(obj2);
    }
    
    /**
     * Compares this object to the given ChangeJar object. The 
     * result is the value 0 if the argument is equal to this 
     * object; a value less than 0 if this object is less than 
     * the argument; and a value greater than 0 if this object 
     * is greater than the argument. The test for "equal" is 
     * based on the total amount in the two jars.
     * 
     * @param other The ChangeJar object to compare this ChangeJar object against
     * 
     * @return the value 0 if the argument is equal to this object; a value 
     * less than 0 if this object is less than the argument; and a value 
     * greater than 0 if this object is greater than the argument.
     */
    public int compareTo(ChangeJar other) {
        int temp = 0;
        if(this.getAmount() > other.getAmount()) {
            temp = 1;
        }
        else if(this.getAmount() < other.getAmount()) {
            temp = -1;
        }
        return temp;
    }

    /**
     * Compares the two given ChangeJar objects. The result is the value 0
     * if the first argument is equal to the second argument; a value less 
     * than 0 if the first object is less than the second argument; and a 
     * value greater than 0 if the first argument is greater than the 
     * second argument. The test for "equal" is based on the total amount 
     * in the two jars.
     * 
     * @param obj1 The ChangeJar object used in comparison
     * @param obj2 The ChangeJar object used in comparison
     * 
     * @return the value 0 if the first argument is equal to the second 
     * argument; a value less than 0 if the first object is less than the 
     * second argument; and a value greater than 0 if the first argument 
     * is greater than the second argument.
     */
    public static int compareTo(ChangeJar obj1, ChangeJar obj2) {
        return obj1.compareTo(obj2);
    }

    /**
     * Deducts the given coins (PAIR-WISE) from this ChangeJar object.
     * 
     * @param quarters number of quarters to deduct from this jar
     * @param dimes number of dimes to deduct from this jar
     * @param nickels number of nickels to deduct from this jar
     * @param pennies number of pennies to deduct from this jar
     * 
     * @throws IllegalArgumentException if the any of parameter values 
     * are negative or not enough coins are in the jar.
     */
    public void subtract(int quarters, int dimes, int nickels, int pennies) {
    	if(globalLock) {
        	return;
        }
    	if(quarters >= 0 && dimes >= 0 && nickels >= 0 && pennies >= 0 &&
    	   this.quarters >= quarters && this.dimes >= dimes &&
    	   this.nickels >= nickels && this.pennies >= 0) {
	        this.quarters -= quarters;
	        this.dimes -= dimes;
	        this.nickels -= nickels;
	        this.pennies -= pennies;
    	} else {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
    }

    /**
     * Deducts the coins (PAIR-WISE) of the specified ChangeJar object 
     * from this ChangeJar object.
     * 
     * @param other the ChangeJar object whose coins are deducted from this object
     * 
     * @throws IllegalArgumentException if the argument is null or operation 
     * is not possible
     */
    public void subtract(final ChangeJar other) {
    	if(globalLock) {
        	return;
        }
    	if(other == null) {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
        this.subtract(other.quarters, other.dimes, other.nickels, other.pennies);
        
    }

    /**
     * Deducts the specified amount from the total amount of this ChangeJar 
     * object in the order of quarters, dimes, nickels, and pennies.
     * 
     * @param amount the amount to deduct from this jar
     * 
     * @throws IllegalArgumentException if the operation is not possible
     */
    public void subtract(final double amount) {
    	if(globalLock) {
        	return;
        }
    	if(amount == 0) {
    		return;
    	}
    	if(this.getAmount() >= amount && isValidDouble(amount)) {
    		double temp = this.getAmount() - amount;
    		converter(temp);
    	} else {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
    }

    /**
     * Deducts one penny from the total amount of this ChangeJar object. 
     * This may affect the number of various coins in the jar.
     * 
     * @throws IllegalArgumentException if this jar has no coins (i.e. empty)
     */
    public void dec() {
    	if(globalLock) {
        	return;
        }
        this.subtract(.01);
        
    }

    /**
     * Adds the specified coins to this ChangeJar object. Updates 
     * the corresponding coins (PAIR-WISE) in the jar.
     * 
     * @param quarters number of quarters to add to this jar
     * @param dimes number of dimes to add to this jar
     * @param nickels number of nickels to add to this jar
     * @param pennies number of pennies to add to this jar
     * 
     * @throws IllegalArgumentException if any of the parameter 
     * values are negative
     */
    public void add(int quarters, int dimes, int nickels, int pennies) {
        if(globalLock) {
        	return;
        }
        if(quarters >= 0 && dimes >= 0 && nickels >= 0 && pennies >= 0) {
	        this.quarters += quarters;
	        this.dimes += dimes;
	        this.nickels += nickels;
	        this.pennies += pennies;
    	} else {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
    }

    /**
     * Adds the coins (PAIR-WISE) of the specified ChangeJar object
     * to this ChangeJar object.
     * 
     * @param other The ChangeJar object whose coins are added 
     * (PAIR-WISE) to this object
     * 
     * @throws IllegalArgumentException if the argument is null
     */
    public void add(ChangeJar other) {
    	if(other != null) {
    		this.add(other.quarters, other.dimes, other.nickels, other.pennies);
    	} else {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
    }

    /**
     * Adds the specified amount to the total amount of this ChangeJar
     * object in the order of quarters, dimes, nickels, and pennies.
     * 
     * @param amount the amount to add to this jar
     * 
     * @throws IllegalArgumentException if the operation is not possible
     */
    public void add(final double amount) {
    	if(globalLock) {
        	return;
        }
    	if(amount == 0) {
    		return;
    	}
    	if(isValidDouble(amount)) {
	    	double temp = this.getAmount() + amount;
	    	converter(temp);
    	} else {
    		throw new IllegalArgumentException("Invalid value for parameter.");
    	}
    }

    /**
     * Increment the total amount of this ChangeJar by one penny. 
     * This may affect the number of various coins in the jar.
     */
    public void inc() {
    	if(globalLock) {
        	return;
        }
        this.add(0,0,0,1);
        converter(this.getAmount());
    }

    /**
     * Returns a String representing the coins in this jar in a readable format
     * 
     * @return a string representation of the value of this ChangeJar object
     */
    public String toString() {
        NumberFormat nm = NumberFormat.getCurrencyInstance();
        String temp = nm.format(this.getAmount()) + ": ";
        if(quarters == 1) {
            temp += quarters + " quarter";
        } 
        else if(quarters > 1) {
            temp += quarters + " quarters";
        }
        if(dimes == 1) {
        	if(quarters >= 1) {
            	temp += ", ";
            }
            temp += dimes + " dime";
        } 
        else if(dimes > 1) {
        	if(quarters >= 1) {
            	temp += ", ";
            }
            temp += dimes + " dimes";
        }
        if(nickels == 1) {
        	if(quarters >= 1 || dimes >= 1) {
            	temp += ", ";
            }
            temp += nickels + " nickel";
        } 
        else if(nickels > 1) {
        	if(quarters >= 1 || dimes >= 1) {
            	temp += ", ";
            }
            temp += nickels + " nickels";
        }
        if(pennies == 1) {
        	if(quarters >= 1 || dimes >= 1 || nickels >= 1) {
            	temp += ", ";
            }
            temp += pennies + " penny";
        } 
        else if(pennies > 1) {
        	if(quarters >= 1 || dimes >= 1 || nickels >= 1) {
            	temp += ", ";
            }
            temp += pennies + " pennies";
        }
        return temp;
    }

    /**
     * Returns an array of size 4 with number of quarters, dimes, nickels, 
     * and pennies currently in the jar.
     * 
     * @return array of size 4 with number of quarters, dimes, nickels, 
     * and pennies currently in the jar
     */
    public int[] toArray() {
        int[] temp = {quarters, dimes, nickels, pennies};
        return temp;
    }

    /**
     * Saves the current state (number of various coins) of this jar 
     * to the file with the specified name
     * 
     * @param filename name of the file where the values of this jar is saved
     */
    public void save(String filename) {
    	if(globalLock) {
        	return;
        }
    	PrintStream out = null;
    	try {
	    	out = new PrintStream(filename);
	    	out.println(this.getQuarters());
			out.println(this.getDimes());
			out.println(this.getNickels());
			out.println(this.getPennies());
			out.close();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
    }

    /**
     * Sets the quarters, dimes, nickels, and pennies of this jar to 
     * the values read from the file specified
     * 
     * @param filename name of the file from which the values for 
     * this jar is read from
     */
    public void load(String filename) {
    	if(globalLock) {
        	return;
        }
    	Scanner file;
		try {
			file = new Scanner( new File(filename) );
			this.quarters = Integer.parseInt(file.nextLine());
			this.dimes = Integer.parseInt(file.nextLine());
			this.nickels = Integer.parseInt(file.nextLine());
			this.pennies = Integer.parseInt(file.nextLine());
		} 
		catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
    }

    /**
     * Locks or unlocks all ChangeJar objects/instances. If the parameter 
     * is true, it locks all ChangeJar objects, unlocks otherwise. When 
     * the lock is on/true, the number of coins in a can't be changed.
     * 
     * @param on true to lock all jars and false to unlock all jars
     */
    public static void lock(boolean on) {
        globalLock = on;
    }
    
    public static void main(String[] args) {
		ChangeJar cj3 = new ChangeJar(10,2,1,4);
		System.out.println(cj3.toString());
		System.out.println(cj3.getAmount());
		cj3.inc();
		System.out.println(cj3.getAmount());
		System.out.println(cj3.toString());
		cj3.dec();
		System.out.println(cj3.getAmount());
		System.out.println(cj3.toString());
		System.out.println("");
		ChangeJar cj2 = new ChangeJar("2.80");
		System.out.println(cj2.toString());
		System.out.println("");
		double temp = Double.parseDouble("2.80");
		System.out.println(temp);
		String temp2 = Double.toString(2.80);
		System.out.println(temp2);
		cj2.save("test.txt");
		System.out.println(cj3.toString());
		cj3.load("test.txt");
		System.out.println(cj3.toString());
		System.out.println("");
		System.out.println("");
		ChangeJar cj1 = new ChangeJar(0.00);
		System.out.println("");
		System.out.println("");
		System.out.println(cj1.toString());
    }
}