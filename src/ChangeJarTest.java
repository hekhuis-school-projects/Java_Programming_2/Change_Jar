import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChangeJarTest {

	/****** TEST CASES FOR BASE FUNCTIONALITY IN METHODS *******/
	
	@Test
	public void testConstructor_1() {
		ChangeJar cj = new ChangeJar();
		assertEquals(0,cj.getQuarters());
		assertEquals(0,cj.getDimes());
		assertEquals(0,cj.getNickels());
		assertEquals(0,cj.getPennies());
	}
	
	@Test
	public void testConstructor_2() {
		ChangeJar cj = new ChangeJar(10,20,25,15);
		assertEquals(10,cj.getQuarters());
		assertEquals(20,cj.getDimes());
		assertEquals(25,cj.getNickels());
		assertEquals(15,cj.getPennies());
	}
	
	@Test
	public void testConstructor_3() {
		ChangeJar cj = new ChangeJar(0,0,0,0);
		assertEquals(0,cj.getQuarters());
		assertEquals(0,cj.getDimes());
		assertEquals(0,cj.getNickels());
		assertEquals(0,cj.getPennies());
	}
	
	@Test
	public void testConstructor_4() {
		ChangeJar cj = new ChangeJar(3.19);
		assertEquals(12,cj.getQuarters());
		assertEquals(1,cj.getDimes());
		assertEquals(1,cj.getNickels());
		assertEquals(4,cj.getPennies());
	}
	
	@Test
	public void testConstructor_5() {
		ChangeJar cj = new ChangeJar("3.19");
		assertEquals(12,cj.getQuarters());
		assertEquals(1,cj.getDimes());
		assertEquals(1,cj.getNickels());
		assertEquals(4,cj.getPennies());
	}
	
	@Test
	public void testConstructor_6() {
		ChangeJar cj1 = new ChangeJar(3.19);
		ChangeJar cj2 = new ChangeJar(cj1);
		assertEquals(12,cj2.getQuarters());
		assertEquals(1,cj2.getDimes());
		assertEquals(1,cj2.getNickels());
		assertEquals(4,cj2.getPennies());
	}
	
	@Test
	public void testEquals_1() {
		ChangeJar cj1 = new ChangeJar(3.25);
		ChangeJar cj2 = new ChangeJar(3.25);
		assertTrue(cj1.equals(cj2));
	}
	
	@Test
	public void testEquals_2() {
		ChangeJar cj1 = new ChangeJar(3.25);
		ChangeJar cj2 = new ChangeJar(5.75);
		assertFalse(cj1.equals(cj2));
	}
	
	@Test
	public void testEquals_3() {
		ChangeJar cj1 = new ChangeJar(3.25);
		Object cj2 = new ChangeJar(3.25);
		assertTrue(cj1.equals(cj2));
	}
	
	@Test
	public void testEquals_4() {
		ChangeJar cj1 = new ChangeJar(3.25);
		Object cj2 = new ChangeJar(5.75);
		assertFalse(cj1.equals(cj2));
	}
	
	@Test
	public void testEquals_5() {
		ChangeJar cj1 = new ChangeJar(3.25);
		ChangeJar cj2 = new ChangeJar(3.25);
		assertTrue(ChangeJar.equals(cj1, cj2));
	}
	
	@Test
	public void testEquals_6() {
		ChangeJar cj1 = new ChangeJar(3.25);
		ChangeJar cj2 = new ChangeJar(5.75);
		assertFalse(ChangeJar.equals(cj1, cj2));
	}
	
	@Test
	public void testCompareTo_1() {
		ChangeJar cj1 = new ChangeJar(3.25);
		ChangeJar cj2 = new ChangeJar(3.25);
		assertTrue(cj1.compareTo(cj2) == 0);
	}
	
	@Test
	public void testCompareTo_2() {
		ChangeJar cj1 = new ChangeJar(2.25);
		ChangeJar cj2 = new ChangeJar(3.25);
		assertTrue(cj1.compareTo(cj2) < 0);
	}
	
	@Test
	public void testCompareTo_3() {
		ChangeJar cj1 = new ChangeJar(5.25);
		ChangeJar cj2 = new ChangeJar(3.25);
		assertTrue(cj1.compareTo(cj2) > 0);
	}
	
	@Test
	public void testCompareTo_4() {
		ChangeJar cj1 = new ChangeJar(3.25);
		ChangeJar cj2 = new ChangeJar(3.25);
		assertTrue(ChangeJar.compareTo(cj1,cj2) == 0);
	}
	
	@Test
	public void testCompareTo_5() {
		ChangeJar cj1 = new ChangeJar(2.25);
		ChangeJar cj2 = new ChangeJar(3.25);
		assertTrue(ChangeJar.compareTo(cj1,cj2) < 0);
	}
	
	@Test
	public void testCompareTo_6() {
		ChangeJar cj1 = new ChangeJar(5.25);
		ChangeJar cj2 = new ChangeJar(3.25);
		assertTrue(ChangeJar.compareTo(cj1,cj2) > 0);
	}
	
	@Test
	public void testSubtract_1() {
		ChangeJar cj1 = new ChangeJar(10,5,8,4);
		cj1.subtract(5, 2, 3, 2);
		assertEquals(5,cj1.getQuarters());
		assertEquals(3,cj1.getDimes());
		assertEquals(5,cj1.getNickels());
		assertEquals(2,cj1.getPennies());
	}
	
	@Test
	public void testSubtract_2() {
		ChangeJar cj1 = new ChangeJar(10,5,8,4);
		ChangeJar cj2 = new ChangeJar(5,2,3,2);
		cj1.subtract(cj2);
		assertEquals(5,cj1.getQuarters());
		assertEquals(3,cj1.getDimes());
		assertEquals(5,cj1.getNickels());
		assertEquals(2,cj1.getPennies());
	}
	
	@Test
	public void testSubtract_3() {
		ChangeJar cj1 = new ChangeJar(10,5,8,4);
		cj1.subtract(2.12);
		assertEquals(5,cj1.getQuarters());
		assertEquals(0,cj1.getDimes());
		assertEquals(1,cj1.getNickels());
		assertEquals(2,cj1.getPennies());
	}
	
	@Test
	public void testSubtract_4() {
		ChangeJar cj1 = new ChangeJar(2,10,15,20);
		cj1.subtract(2.12);
		assertEquals(1,cj1.getQuarters());
		assertEquals(0,cj1.getDimes());
		assertEquals(1,cj1.getNickels());
		assertEquals(3,cj1.getPennies());
	}
	
	@Test
	public void testSubtract_5() {
		ChangeJar cj1 = new ChangeJar(2,10,15,20);
		cj1.subtract(0.0);
		assertEquals(2,cj1.getQuarters());
		assertEquals(10,cj1.getDimes());
		assertEquals(15,cj1.getNickels());
		assertEquals(20,cj1.getPennies());
	}
	
	@Test
	public void testDec_1() {
		ChangeJar cj1 = new ChangeJar(10,5,8,4);
		cj1.dec();
		assertEquals(13,cj1.getQuarters());
		assertEquals(1,cj1.getDimes());
		assertEquals(1,cj1.getNickels());
		assertEquals(3,cj1.getPennies());
	}
	
	@Test
	public void testDec_2() {
		ChangeJar cj1 = new ChangeJar(10,5,8,0);
		cj1.dec();
		assertEquals(13,cj1.getQuarters());
		assertEquals(1,cj1.getDimes());
		assertEquals(0,cj1.getNickels());
		assertEquals(4,cj1.getPennies());
	}
	
	@Test
	public void testDec_3() {
		ChangeJar cj1 = new ChangeJar(10,5,0,0);
		cj1.dec();
		assertEquals(11,cj1.getQuarters());
		assertEquals(2,cj1.getDimes());
		assertEquals(0,cj1.getNickels());
		assertEquals(4,cj1.getPennies());
	}
	
	@Test
	public void testDec_4() {
		ChangeJar cj1 = new ChangeJar(10,0,0,0);
		cj1.dec();
		assertEquals(9,cj1.getQuarters());
		assertEquals(2,cj1.getDimes());
		assertEquals(0,cj1.getNickels());
		assertEquals(4,cj1.getPennies());
	}
	
	@Test
	public void testAdd_1() {
		ChangeJar cj1 = new ChangeJar(10,5,8,4);
		cj1.add(5, 2, 3, 2);
		assertEquals(15,cj1.getQuarters());
		assertEquals(7,cj1.getDimes());
		assertEquals(11,cj1.getNickels());
		assertEquals(6,cj1.getPennies());
	}
	
	@Test
	public void testAdd_2() {
		ChangeJar cj1 = new ChangeJar(10,5,8,4);
		ChangeJar cj2 = new ChangeJar(5,2,3,2);
		cj1.add(cj2);
		assertEquals(15,cj1.getQuarters());
		assertEquals(7,cj1.getDimes());
		assertEquals(11,cj1.getNickels());
		assertEquals(6,cj1.getPennies());
	}
	
	@Test
	public void testAdd_3() {
		ChangeJar cj1 = new ChangeJar(10,5,8,4);
		cj1.add(2.12);
		assertEquals(22,cj1.getQuarters());
		assertEquals(0,cj1.getDimes());
		assertEquals(1,cj1.getNickels());
		assertEquals(1,cj1.getPennies());
	}
	
	@Test
	public void testAdd_4() {
		ChangeJar cj1 = new ChangeJar(1,2,3,4);
		cj1.add(1.18);
		assertEquals(7,cj1.getQuarters());
		assertEquals(0,cj1.getDimes());
		assertEquals(1,cj1.getNickels());
		assertEquals(2,cj1.getPennies());
	}
	
	@Test
	public void testAdd_5() {
		ChangeJar cj1 = new ChangeJar(1,2,3,4);
		cj1.add(0.0);
		assertEquals(1,cj1.getQuarters());
		assertEquals(2,cj1.getDimes());
		assertEquals(3,cj1.getNickels());
		assertEquals(4,cj1.getPennies());
	}
	
	@Test
	public void testInc_1() {
		ChangeJar cj1 = new ChangeJar(10,5,8,4);
		cj1.inc();
		assertEquals(13,cj1.getQuarters());
		assertEquals(2,cj1.getDimes());
		assertEquals(0,cj1.getNickels());
		assertEquals(0,cj1.getPennies());
	}
	
	@Test
	public void testInc_2() {
		ChangeJar cj1 = new ChangeJar(10,2,1,4);
		cj1.inc();
		assertEquals(11,cj1.getQuarters());
		assertEquals(0,cj1.getDimes());
		assertEquals(1,cj1.getNickels());
		assertEquals(0,cj1.getPennies());
	}
	
	@Test
	public void testToArray_1() {
		ChangeJar cj1 = new ChangeJar();
		int[] coins = cj1.toArray();
		assertNotNull(coins);
		assertEquals(4,coins.length);
		assertEquals(0,coins[0]);
		assertEquals(0,coins[1]);
		assertEquals(0,coins[2]);
		assertEquals(0,coins[3]);
	}
	
	@Test
	public void testToArray_2() {
		ChangeJar cj1 = new ChangeJar(10,2,1,4);
		int[] coins = cj1.toArray();
		assertNotNull(coins);
		assertEquals(4,coins.length);
		assertEquals(10,coins[0]);
		assertEquals(2,coins[1]);
		assertEquals(1,coins[2]);
		assertEquals(4,coins[3]);
	}
	
	@Test
	public void testToString_1() {
		ChangeJar cj = new ChangeJar(10,2,1,4);
		assertEquals("$2.79: 10 quarters, 2 dimes, 1 nickel, 4 pennies",cj.toString().trim());
	}
	
	@Test
	public void testToString_2() {
		ChangeJar cj = new ChangeJar(0,2,0,4);
		assertEquals("$0.24: 2 dimes, 4 pennies",cj.toString().trim());
	}
	
	@Test
	public void testToString_3() {
		ChangeJar cj = new ChangeJar(3,2,5,0);
		assertEquals("$1.20: 3 quarters, 2 dimes, 5 nickels",cj.toString().trim());
	}
	
	@Test
	public void testToString_4() {
		ChangeJar cj = new ChangeJar(3,0,0,5);
		assertEquals("$0.80: 3 quarters, 5 pennies",cj.toString().trim());
	}
	
	/****** TEST CASES FOR ERROR CHECKING IN METHODS *******/
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_1() {
		new ChangeJar(-5,5,5,5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_2() {
		new ChangeJar(5,-5,5,5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_3() {
		new ChangeJar(5,5,-5,5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_4() {
		new ChangeJar(5,5,5,-5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_5() {
		new ChangeJar(-5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_6() {
		new ChangeJar(-5.75);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_7() {
		new ChangeJar("-5.75");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_8() {
		new ChangeJar("HELLO");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_9() {
		new ChangeJar("123HELLO");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_10() {
		new ChangeJar("HELLO123");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_11() {
		new ChangeJar("$12.35");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_12() {
		new ChangeJar("12.34.56");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_13() {
		new ChangeJar("12.345");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_14() {
		new ChangeJar("");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_15() {
		String str = null;
		new ChangeJar(str);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_16() {
		ChangeJar cj = null;
		new ChangeJar(cj);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testConstructor_Error_17() {
		new ChangeJar(12.756);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAdd_Error_1() {
		ChangeJar cj = new ChangeJar(3.25);
		cj.add(-5, 5, 5, 5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAdd_Error_2() {
		ChangeJar cj = new ChangeJar(3.25);
		cj.add(5, -5, 5, 5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAdd_Error_3() {
		ChangeJar cj = new ChangeJar(3.25);
		cj.add(5, 5, -5, 5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAdd_Error_4() {
		ChangeJar cj = new ChangeJar(3.25);
		cj.add(5, 5, 5, -5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAdd_Error_5() {
		ChangeJar cj = new ChangeJar(3.25);
		cj.add(-1.25);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAdd_Error_6() {
		ChangeJar cj = new ChangeJar(3.25);
		cj.add(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSubtract_Error_1() {
		ChangeJar cj = new ChangeJar(5.25);
		cj.subtract(-5, 5, 5, 5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSubtract_Error_2() {
		ChangeJar cj = new ChangeJar(5.25);
		cj.subtract(5, -5, 5, 5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSubtract_Error_3() {
		ChangeJar cj = new ChangeJar(5.25);
		cj.subtract(5, 5, -5, 5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSubtract_Error_4() {
		ChangeJar cj = new ChangeJar(5.25);
		cj.subtract(5, 5, 5, -5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSubtract_Error_5() {
		ChangeJar cj = new ChangeJar(5.25);
		cj.subtract(-3.25);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSubtract_Error_6() {
		ChangeJar cj = new ChangeJar(5.25);
		cj.subtract(7.25);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSubtract_Error_7() {
		ChangeJar cj = new ChangeJar(5.25);
		cj.subtract(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSubtract_Error_8() {
		ChangeJar cj1 = new ChangeJar(5.25);
		ChangeJar cj2 = new ChangeJar(7.00);
		cj1.subtract(cj2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSubtract_Error_9() {
		ChangeJar cj = new ChangeJar();
		cj.dec();
	}
	
	/****** TEST CASES FOR SAVE, LOAD, AND LOCK METHODS *******/
	
	@Test
	public void testSaveLoad() {
		ChangeJar cj1 = new ChangeJar(10,2,1,4);
		cj1.save("changejar.txt");
		cj1.add(1,2,3,4);
		assertEquals(11,cj1.getQuarters());
		assertEquals(4,cj1.getDimes());
		assertEquals(4,cj1.getNickels());
		assertEquals(8,cj1.getPennies());
		cj1.load("changejar.txt");
		assertEquals(10,cj1.getQuarters());
		assertEquals(2,cj1.getDimes());
		assertEquals(1,cj1.getNickels());
		assertEquals(4,cj1.getPennies());		
	}
	
	@Test
	public void testLock_1() {
		ChangeJar cj1 = new ChangeJar(2,2,2,2);
		ChangeJar cj2 = new ChangeJar(4,4,4,4);
		ChangeJar cj3 = new ChangeJar(6,6,6,6);
		
		ChangeJar.lock(true);
		
		// perform few add operations on jars
		cj1.add(1,1,1,1);
		cj2.add(1.50);
		cj3.add(cj1);
		cj1.inc();
		cj2.inc();
		cj3.inc();
		
		assertEquals(2,cj1.getQuarters());
		assertEquals(2,cj1.getDimes());
		assertEquals(2,cj1.getNickels());
		assertEquals(2,cj1.getPennies());

		assertEquals(4,cj2.getQuarters());
		assertEquals(4,cj2.getDimes());
		assertEquals(4,cj2.getNickels());
		assertEquals(4,cj2.getPennies());
		
		assertEquals(6,cj3.getQuarters());
		assertEquals(6,cj3.getDimes());
		assertEquals(6,cj3.getNickels());
		assertEquals(6,cj3.getPennies());
		
		ChangeJar.lock(false);
	}	
	
	@Test
	public void testLock_2() {
		ChangeJar cj1 = new ChangeJar(2,2,2,2);
		ChangeJar cj2 = new ChangeJar(4,4,4,4);
		ChangeJar cj3 = new ChangeJar(6,6,6,6);
		
		ChangeJar.lock(true);
		
		// perform few subtraction operations on jars
		cj1.subtract(1,1,1,1);
		cj2.subtract(1.50);
		cj3.subtract(cj1);
		cj1.dec();
		cj2.dec();
		cj3.dec();
		
		assertEquals(2,cj1.getQuarters());
		assertEquals(2,cj1.getDimes());
		assertEquals(2,cj1.getNickels());
		assertEquals(2,cj1.getPennies());

		assertEquals(4,cj2.getQuarters());
		assertEquals(4,cj2.getDimes());
		assertEquals(4,cj2.getNickels());
		assertEquals(4,cj2.getPennies());
		
		assertEquals(6,cj3.getQuarters());
		assertEquals(6,cj3.getDimes());
		assertEquals(6,cj3.getNickels());
		assertEquals(6,cj3.getPennies());
		
		ChangeJar.lock(false);
	}	

	@Test
	public void testLock_3() {
		ChangeJar cj1 = new ChangeJar(2,2,2,2);
		ChangeJar cj2 = new ChangeJar();
		ChangeJar cj3 = new ChangeJar();
				
		ChangeJar.lock(true);
		
		cj1.save("cj.txt");
		cj2.load("cj.txt");
		cj3.load("cj.txt");
		
		assertEquals(2,cj1.getQuarters());
		assertEquals(2,cj1.getDimes());
		assertEquals(2,cj1.getNickels());
		assertEquals(2,cj1.getPennies());

		assertEquals(0,cj2.getQuarters());
		assertEquals(0,cj2.getDimes());
		assertEquals(0,cj2.getNickels());
		assertEquals(0,cj2.getPennies());
		
		assertEquals(0,cj3.getQuarters());
		assertEquals(0,cj3.getDimes());
		assertEquals(0,cj3.getNickels());
		assertEquals(0,cj3.getPennies());
		
		ChangeJar.lock(false);
	}	
}