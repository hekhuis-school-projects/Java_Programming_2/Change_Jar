Change Jar
Language: Java
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 163 - Java Programming 2
Semester / Year: Winter 2015

This program creates change jars that you can add or subtract money from.
The jars may also be locked so changes can not be made to them.

Usage:
Run the main method in 'MyChangeJarPanel' for a GUI. Enter in the amount to
either add or subtract from the change jar as # or #.## (no $). Then click the
respective button. You can lock the jars so that no changes may be made to
them using the "Lock Jars" button and unlock with the "Unlock Jars" button.